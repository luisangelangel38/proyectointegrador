import networkx as nx
import time
from heapq import heappop, heappush
from itertools import count


def bidirectional_dijkstra(G, source, target, weight="weight"):
    if source not in G or target not in G:
        msg = f"Either source {source} or target {target} is not in G"
        raise nx.NodeNotFound(msg)

    if source == target:
        return (0, [source])

    push = heappush
    pop = heappop
    dists = [{}, {}]
    paths = [{source: [source]}, {target: [target]}]
    fringe = [[], []]
    seen = [{source: 0}, {target: 0}]
    c = count()
    push(fringe[0], (0, next(c), source))
    push(fringe[1], (0, next(c), target))

    if G.is_directed():
        neighs = [G._succ, G._pred]
    else:
        neighs = [G._adj, G._adj]

    finalpath = []
    dir = 1

    while fringe[0] and fringe[1]:
        dir = 1 - dir
        (dist, _, v) = pop(fringe[dir])

        if v in dists[dir]:
            continue

        dists[dir][v] = dist

        if v in dists[1 - dir]:
            return (finaldist, finalpath)

        for w, d in neighs[dir][v].items():
            if isinstance(weight, str):
                cost = d.get(weight, 1)
            else:
                cost = weight(v, w, d) if dir == 0 else weight(w, v, d)
            if cost is None:
                continue
            vwLength = dists[dir][v] + cost
            if w in dists[dir]:
                if vwLength < dists[dir][w]:
                    raise ValueError("Contradictory paths found: negative weights?")
            elif w not in seen[dir] or vwLength < seen[dir][w]:
                seen[dir][w] = vwLength
                push(fringe[dir], (vwLength, next(c), w))
                paths[dir][w] = paths[dir][v] + [w]
                if w in seen[0] and w in seen[1]:
                    totaldist = seen[0][w] + seen[1][w]
                    if finalpath == [] or finaldist > totaldist:
                        finaldist = totaldist
                        revpath = paths[1][w][:]
                        revpath.reverse()
                        finalpath = paths[0][w] + revpath[1:]
    raise nx.NetworkXNoPath(f"No path between {source} and {target}.")

# Creamos un grafo más grande
G = nx.complete_graph(20)

# Definimos el nodo de origen y el nodo de destino
source = 0  # El nodo más pequeño en el grafo es el nodo 0
target = 19  # El nodo más grande en el grafo es el nodo 19

# Medimos el tiempo de ejecución para bidirectional_dijkstra
start_time = time.time()

# Llamamos a la función y obtenemos el resultado
length, path = bidirectional_dijkstra(G, source, target)

end_time = time.time()

# Calculamos el tiempo transcurrido en milisegundos con 4 decimales
execution_time_dijkstra = (end_time - start_time) * 1000

# Imprimimos el resultado de bidirectional_dijkstra
print("Bidirectional Dijkstra:")
print("Distancia más corta:", length)
print("Camino más corto:", path)
print("Tiempo de ejecución: {:.4f} milisegundos".format(execution_time_dijkstra))

def shortest_path_with_floyd_warshall(G, source, target):
    # Calculamos las distancias más cortas entre todos los pares de nodos
    dist = nx.floyd_warshall(G)

    # Reconstruimos el camino más corto
    pred = nx.floyd_warshall_predecessor(G)

    # Inicializamos la lista del camino
    path = [source]

    # Construimos el camino retrocediendo desde el destino hasta el origen
    while target != source:
        path.insert(0, pred[source][target])
        target = pred[source][target]

    return dist[source][target], path

# Medimos el tiempo de ejecución para shortest_path_with_floyd_warshall
start_time = time.time()

# Llamamos a la función shortest_path_with_floyd_warshall para encontrar el camino más corto
length, path = shortest_path_with_floyd_warshall(G, source, target)

end_time = time.time()

# Calculamos el tiempo transcurrido en milisegundos con 4 decimales
execution_time_floyd = (end_time - start_time) * 1000

# Imprimimos el resultado de shortest_path_with_floyd_warshall
print("\nFloyd-Warshall:")
print("Distancia más corta:", length)
print("Camino más corto:", path)
print("Tiempo de ejecución: {:.4f} milisegundos".format(execution_time_floyd))
