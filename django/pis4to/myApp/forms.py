from django import forms
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
from .models import InformationCatalog
from ckeditor.widgets import CKEditorWidget
from django import forms
from .models import EmergencyPlan


class SignUpForm(UserCreationForm):
    email = forms.EmailField(
        max_length=254, help_text='Campo requerido. Ingrese una dirección de correo electrónico válida.')

    class Meta:
        model = User
        fields = ['username', 'email', 'password1', 'password2']


class InformationCatalogForm(forms.ModelForm):
    class Meta:
        model = InformationCatalog
        fields = ['title', 'description', 'status']

    description = forms.CharField(widget=CKEditorWidget())

    # Utiliza el widget de ChoiceField para el campo status
    status = forms.ChoiceField(choices=InformationCatalog.STATUS_CHOICES,
                               widget=forms.Select(attrs={'class': 'form-control'}))


class EmergencyPlanForm(forms.ModelForm):
    class Meta:
        model = EmergencyPlan
        fields = ['name', 'link', 'status']

    # Utiliza el widget de TextInput para el campo name y link
    name = forms.CharField(widget=forms.TextInput(
        attrs={'class': 'form-control'}))
    link = forms.CharField(widget=forms.TextInput(
        attrs={'class': 'form-control'}))

    # Utiliza el widget de ChoiceField para el campo status
    status = forms.ChoiceField(choices=EmergencyPlan.STATUS_CHOICES,
                               widget=forms.Select(attrs={'class': 'form-control'}))
