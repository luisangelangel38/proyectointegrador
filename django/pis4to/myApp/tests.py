from django.test import TestCase
from pis4to.wsgi import *

from django.db import models
from myApp.models import Point

data = [
    {"name": "Laboratorio de micropropagación vegetal",
        "latitude": -4.032255, "longitude": -79.200031},
    {"name": "S/N", "latitude": -4.0337353, "longitude": -79.1997873},
    {"name": "Administración quinta la Argelia",
        "latitude": -4.0330433, "longitude": -79.1997240},
    {"name": "Invernadero", "latitude": -4.0330142, "longitude": -79.1997384},
    {"name": "Bodega", "latitude": -4.0335547, "longitude": -79.1997803},
    {"name": "Bodega", "latitude": -4.0334450, "longitude": -79.1995469},
    {"name": "Banco de germoplastia", "latitude": -
        4.0336440, "longitude": -79.1994115},
    {"name": "Maquinaria agrícola y aserrío",
        "latitude": -4.0343145, "longitude": -79.1996465},
    {"name": "Laboratorio física-mecánica de madera",
        "latitude": -4.03615, "longitude": -79.20360},
    {"name": "Laboratorios de suelos", "latitude": -4.03524, "longitude": -79.20314},
    {"name": "Bloque de aulas", "latitude": -4.0352239, "longitude": -79.203381},
    {"name": "Invernadero", "latitude": -4.03547, "longitude": -79.20358},
    {"name": "Laboratorio de fisiología vegetal",
        "latitude": -4.035557, "longitude": -79.20330},
    {"name": "HERBARIO ''REINALDO ESPINOSA''w",
        "latitude": -4.03563, "longitude": -79.20326},
    {"name": "LABORATORIO DE BROMATOLOGÍA, QUÍMICA Y DASOMETRÍA",
        "latitude": -4.03542, "longitude": -79.20325},
    {"name": "CARRERA DE AGRONOMÍA", "latitude": -4.03538, "longitude": -79.20297},
    {"name": "ÁREA ADMINISTRATIVA", "latitude": -4.0351600, "longitude": -79.2028430},
    {"name": "AULA MAGNA", "latitude": -4.0351056, "longitude": -79.2029277},
    {"name": "SALA DE DOCENTES", "latitude": -4.0353874, "longitude": -79.2027522},
    {"name": "BIBLIOTECA", "latitude": -4.0353755, "longitude": -79.2028167},
    {"name": "EX CERACYT", "latitude": -4.0354379, "longitude": -79.2027800},
    {"name": "CAFETERÍA", "latitude": -4.0354587, "longitude": -79.2028655},
    {"name": "CARRERA DE VETERINARIA", "latitude": -4.03584, "longitude": -79.20335},
    {"name": "LABORATORIO-DIAGNOSTICO VETERINARIO",
        "latitude": -4.03591, "longitude": -79.20309},
    {"name": "ANFITEATRO DE VETERINARIA",
        "latitude": -4.03608, "longitude": -79.20332},
    {"name": "LABORATORIO DE APICULTURA",
        "latitude": -4.03617, "longitude": -79.20326},
    {"name": "GUARDA ALMACEN", "latitude": -4.03614, "longitude": -79.20318},
    {"name": "LABORATORIO AMBIENTAL", "latitude": -4.03635, "longitude": -79.20320},
    {"name": "LABORATORIO DE PATALOGÍA VETERINARIA",
        "latitude": -4.03638, "longitude": -79.20308},
    {"name": "HOSPITAL VETERINARIO", "latitude": -4.03588, "longitude": -79.20298},
    {"name": "AULAS DE VETERINARIA", "latitude": -4.03633, "longitude": -79.20292},
    {"name": "CAFETERÍA", "latitude": -4.03619, "longitude": -79.20273},
    {"name": "ADMINISTRACIÓN Y POST GRADO",
        "latitude": -4.03599, "longitude": -79.20274},
    {"name": "CARRERA INGENIERÍA FORESTAL",
        "latitude": -4.03577, "longitude": -79.20395},
    {"name": "CARRERA DE AGRICOLA", "latitude": -4.03593, "longitude": -79.20382},
    {"name": "CARRERA DE MEDIO AMBIENTE",
        "latitude": -4.03615, "longitude": -79.20360},
    {"name": "ADMINISTRACIÓN F.J.S.A.", "latitude": -4.03558, "longitude": -79.20488},
    {"name": "CARRERA DE TRABAJO SOCIAL",
        "latitude": -4.03585, "longitude": -79.20493},
    {"name": "CARRERA DE DERECHO", "latitude": -4.03571, "longitude": -79.20458},
    {"name": "INNOVATION COOWORKING", "latitude": -4.03602, "longitude": -79.20473},
    {"name": "BLOQUE DE AULAS CARRERA DE DERECHO",
        "latitude": -4.03606, "longitude": -79.20430},
    {"name": "AULA MAGNA Y CARRERA DE DERECHO",
        "latitude": -4.03614, "longitude": -79.20446},
    {"name": "BATERIAS SANITARIAS F.J.S.A.",
        "latitude": -4.03627, "longitude": -79.20443},
    {"name": "BIBLIOTECA F.J.S.A.", "latitude": -4.03630, "longitude": -79.20423},
    {"name": "CARRERA DE ADMINISTRACIÓN PÚBLICA",
        "latitude": -4.03623, "longitude": -79.20534},
    {"name": "SECCIÓN ADMINISTRATIVA Y ACADÉMICA DE DERECHO",
        "latitude": -4.03615, "longitude": -79.20511},
    {"name": "BATERIAS SANITARIAS F.J.S.A.",
        "latitude": -4.03639, "longitude": -79.20522},
    {"name": "CARRERA DE BANCA Y FINANZAS",
        "latitude": -4.03641, "longitude": -79.20514},
    {"name": "CARRERA DE COMUNICACIÓN SOCIAL",
        "latitude": -4.03664, "longitude": -79.20521},
    {"name": "CARRERA DE ECONOMÍA", "latitude": -4.03658, "longitude": -79.20492},
    {"name": "CAFETERÍA F.J.S.A.", "latitude": -4.03652, "longitude": -79.20473},
    {"name": "BATERIAS SANITARIAS F.J.S.A.",
        "latitude": -4.0369995, "longitude": -79.2049657},
    {"name": "CARRERA DE ADMINISTRACIÓN DE EMPRESAS",
        "latitude": -4.036982, "longitude": -79.204637},
    {"name": "Obelisco UNL", "latitude": -4.0370610, "longitude": -79.2044567},
    {"name": "CARRERA DE CONTABILIDAD Y AUDITORÍA",
        "latitude": -4.037131, "longitude": -79.204293},
    {"name": "CARRERA DE ADMINISTRACIÓN TURISTICA",
        "latitude": -4.0373771, "longitude": -79.2047296},
    {"name": "NIVEL DE POST GRADO F.J.S.A",
        "latitude": -4.0374814, "longitude": -79.2045137},
    {"name": "RADIO UNIVERSITARIA Y CUBÍCULOS DE DOCENTES AGRÍCOLA",
        "latitude": -4.037697, "longitude": -79.203768},
    {"name": "AULAS CARRERA DE FORESTAL",
        "latitude": -4.037554, "longitude": -79.203665},
    {"name": "Redondel", "latitude": -4.032785, "longitude": -79.202424},
    {"name": "Nodo Aux", "latitude": -4.032968, "longitude": -79.202549},
    {"name": "Nodo Aux", "latitude": -4.033454, "longitude": -79.202454},
    {"name": "Nodo Aux", "latitude": -4.033854, "longitude": -79.203399},
    {"name": "Nodo Aux", "latitude": -4.0331004, "longitude": -79.203477},
    {"name": "Nodo Aux", "latitude": -4.03352, "longitude": -79.203455},
    {"name": "Nodo Aux", "latitude": -4.033607, "longitude": -79.203571},
    {"name": "Nodo Aux", "latitude": -4.034134, "longitude": -79.203399},
    {"name": "Nodo Aux", "latitude": -4.034303, "longitude": -79.203402},
    {"name": "Biblioteca", "latitude": -4.034279, "longitude": -79.203405},
    {"name": "Punto Seguro", "latitude": -4.0373068, "longitude": -79.2035626},
    {"name": "Punto Seguro", "latitude": -4.0372861, "longitude": -79.2054911},
    {"name": "Punto Seguro", "latitude": -4.0360403, "longitude": -79.2040236},
    {"name": "Punto Seguro", "latitude": -4.0357998, "longitude": -79.2051839},
    {"name": "Punto Seguro", "latitude": -4.0353844, "longitude": -79.2043850},
    {"name": "Punto Seguro", "latitude": -4.0366098, "longitude": -79.2031753},
]

for item in data:
    point = Point(
        name=item["name"], longitude=item["longitude"], latitude=item["latitude"])
    point.save()

#aqui algoritmos necesarios para el proyecto
from myApp.models import Point
import math







punto_inicial = Point.objects.get(name='Maquinaria agrícola y aserrío')
resultado = punto_inicial.shortest_path_to_safest_point()
print(resultado)
