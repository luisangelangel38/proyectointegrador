importScripts('https://www.gstatic.com/firebasejs/3.9.0/firebase-app.js');
importScripts('https://www.gstatic.com/firebasejs/3.9.0/firebase-messaging.js');

var firebaseConfig = {
    apiKey: "AIzaSyAeWVYibeymGKuz9muQpBLgBJNX7iIRmlM",
    authDomain: "evacuationrutes.firebaseapp.com",
    projectId: "evacuationrutes",
    storageBucket: "evacuationrutes.appspot.com",
    messagingSenderId: "282539804766",
    appId: "1:282539804766:web:491a01e4069d98439d4ccf"
  };
  
  // Initialize Firebase
  firebase.initializeApp(firebaseConfig);

  let messaging = firebase.messaging();

  messaging.setBackgroundMessageHandler(function(payload){
    let data = payload;
    let title = payload.notification.title;
    let option ={
      body: payload.notification.body,
      icon: payload.notification.icon,
    }
    self.registration.showNotification(title, option);

  });

self.addEventListener('notificationclick', function(event) {
    event.notification.close();
    event.waitUntil(
        clients.openWindow(event.notification.data.click_action)
    );
});