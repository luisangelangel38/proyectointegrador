from models import Point
import math


def shortest_path_to_safest_point(self):
        safest_points = Point.objects.filter(name='punto seguro')

        if not safest_points.exists():
            return "No se encontraron puntos seguros en la base de datos."

        def distance(point1, point2):
            return math.sqrt((point1.latitude - point2.latitude) ** 2 +
                             (point1.altitude - point2.altitude) ** 2)

        min_distance = float('inf')
        closest_safest_point = None

        for sp in safest_points:
            dist = distance(self, sp)
            if dist < min_distance:
                min_distance = dist
                closest_safest_point = sp

        if closest_safest_point:
            return f"El punto seguro más cercano es: {closest_safest_point.name}"
        else:
            return "No se pudo encontrar un punto seguro cercano."

